using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamepadController : MonoBehaviour {

    public static GamepadController controller;

    public bool enabled = false;
    private IEnumerator checkInputState;

    void Awake() {
        // singleton pattern
        if (controller == null) {
            controller = this;
        } else if (controller != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        checkInputState = CheckInputState();
        StartCoroutine(checkInputState);
    }

    void Update() {
        if (!enabled) {
            return;
        }
        if (Input.GetKeyDown(GetKeyCode("JoystickButton4"))) {
            SelectorManager.manager.SelectPrevious();
        }
        if (Input.GetKeyDown(GetKeyCode("JoystickButton5"))) {
            SelectorManager.manager.SelectNext();
        }
    }

    KeyCode GetKeyCode(string button) {
        return (KeyCode)System.Enum.Parse(typeof(KeyCode), button);
    }

    IEnumerator CheckInputState() {
        while (true) {
            if (!enabled) {
                foreach(System.Enum val in System.Enum.GetValues(typeof(GamepadButtons))) {
                    KeyCode kc = (KeyCode)System.Enum.Parse(typeof(KeyCode), val.ToString());
                    if(Input.GetKeyDown(kc)) {
                        enabled = true;
                        Cursor.visible = !enabled;
                        yield return null;
                    }
                }
                yield return null;
            }

            if (enabled) {
                foreach(System.Enum val in System.Enum.GetValues(typeof(KeyboardButtons))) {
                    KeyCode kc = (KeyCode)System.Enum.Parse(typeof(KeyCode), val.ToString());
                    if(Input.GetKeyDown(kc)) {
                        enabled = false;
                        Cursor.visible = !enabled;
                    }
                }
            }
            yield return null;
        }
    }
}
