using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Selector : EventTrigger {

    public override void OnPointerClick(PointerEventData eventData) {
        SelectorManager.manager.HandleSelection(transform);
    }
}
