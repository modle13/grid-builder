using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VillagerController : MonoBehaviour {

    private float moveRecheck = 0.0f;
    private float lastMoveCheck = 10.0f;
    private float max = 5.0f;
    private int speed = 10;
    public Vector3 moveForce;
    private Rigidbody rigidBody;

    void Start() {
        moveForce = GetRandomVector();
        SetCheckTimes();
        rigidBody = GetComponent<Rigidbody>();
    }

    void Update () {
        if (GetComponent<Targets>().target == null) {
            UpdateVectorForIdleVillagers();
        }
        rigidBody.AddForce(moveForce.normalized * speed);
        Vector3 newRotation = new Vector3(rigidBody.velocity.x, 0, rigidBody.velocity.z);
        if (newRotation != Vector3.zero) {
            transform.rotation = Quaternion.LookRotation(newRotation);
        }
    }

    public void Move(GameObject target) {
        if (GetComponent<CollisionState>().repelled) {
            return;
        }
        moveForce = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z) - transform.position;
    }

    void UpdateVectorForIdleVillagers() {
        if (GetComponent<Properties>().currentState != "") {
            return;
        }
        if (Time.time - lastMoveCheck > moveRecheck) {
            moveForce = GetRandomVector();
            SetCheckTimes();
        }
    }

    Vector3 GetRandomVector() {
        return new Vector3(GetRandomOneMinusOne(), 0, GetRandomOneMinusOne());
    }

    int GetRandomOneMinusOne() {
        return Random.Range(0, 2) * 2 - 1;
    }

    void SetCheckTimes() {
        moveRecheck = Random.Range(0.5f, 2.0f);
        lastMoveCheck = Time.time;
    }

    public void RemoveForce() {
        moveForce = Vector3.zero;
    }
}
