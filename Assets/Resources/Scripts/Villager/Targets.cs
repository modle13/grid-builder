using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * This object is attached to the villager prefab
 */
public class Targets : MonoBehaviour {
    public GameObject target;
    private Vector3 lastPosition = new Vector3(0, 0, 0);

    public int CountAvailableTargets() {
        int count = 0;
        foreach (GameObject go in TargetBucket.bucket.targets) {
            if (go == null) {
                continue;
            }
            Properties compareProps = go.GetComponent<Properties>();
            if (compareProps.targeted) {
                continue;
            }
            count++;
        }
        return count;
    }

    void Update () {
        if (HasTarget()) {
            Debug.DrawLine(transform.position, target.transform.position, Color.white);
        }
    }

    public bool HasTarget() {
        if (target == null) {
            GetClosest();
        }
        if (target != null) {
            return true;
        }
        return false;
    }

    private void GetClosest() {
        if (target != null) {
            return;
        }
        GameObject closest = CompareToTargets(TargetBucket.bucket.targets);
        if (closest != null) {
            target = closest;
            target.GetComponent<Properties>().SetTargeted(GetComponent<Properties>().id);
            if (target.GetComponent<Properties>().type == "building") {
                GetComponent<CollisionState>().ProcessCollision(target);
            }
        }
    }

    private GameObject CompareToTargets(List<GameObject> targets) {
        Vector3 position = transform.position;
        float distance = Mathf.Infinity;
        GameObject match = null;
        foreach (GameObject go in targets) {
            if (go == null) {
                continue;
            }
            Properties compareProps = go.GetComponent<Properties>();
            if (!compareProps.selected || compareProps.targeted || compareProps.job != GetComponent<Properties>().job) {
                continue;
            }
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance) {
                match = go;
                distance = curDistance;
            }
        }
        return match;
    }

    public void DecomposeTarget() {
        if (target.GetComponent<Properties>().destructable) {
            Destroy(target);
            GetComponent<CollisionState>().lastCollisionRecheck = Time.time;
        }
        target = GetClosestStorage();
    }

    public GameObject GetClosestStorage() {
        Vector3 position = transform.position;
        float distance = Mathf.Infinity;
        GameObject match = null;
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("storage")) {
            if (go == null) {
                continue;
            }
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance) {
                match = go;
                distance = curDistance;
            }
        }
        return match;
    }

}
