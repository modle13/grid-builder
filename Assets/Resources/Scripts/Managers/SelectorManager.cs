using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectorManager : MonoBehaviour {

    public static SelectorManager manager;
    private Color panelOn;
    private Color panelOff;
    List<string> hideThese = new List<string>() {"sawyer"};
    List<string> order = new List<string>() {"tree", "rock", "none", "sawyer", "storage"};
    private int currentSelection = 2;

    void Awake() {
        // singleton pattern
        if (manager == null) {
            manager = this;
        } else if (manager != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        SetPanelColors();
        HideSelectors();
    }

    void SetPanelColors() {
        ColorUtility.TryParseHtmlString("#09FF0064", out panelOn);
        ColorUtility.TryParseHtmlString("#FFFFFF7F", out panelOff);
    }

    private void HideSelectors() {
        TargetManager.manager.StopSelectionCoroutine();
        foreach (Transform t in transform) {
            if (hideThese.Contains(t.name)) {
                t.gameObject.SetActive(false);
            }
        }
    }

    public void EnableSelector(string target) {
        foreach (Transform t in transform) {
            if (t.name == target) {
                t.gameObject.SetActive(true);
                return;
            }
        }
    }

    public void SelectNext() {
        HideSelectors();
        GetNext();
        HandleSelection(transform.Find(order[currentSelection]));
    }

    public void SelectPrevious() {
        HideSelectors();
        GetPrevious();
        HandleSelection(transform.Find(order[currentSelection]));
    }

    public void HandleSelection(Transform transform) {
        TargetManager.manager.SetTarget(transform.name);
        CursorManager.manager.SetTarget(transform.name);
        SetOtherSelectorsDefaultColor(transform);
    }

    private void SetOtherSelectorsDefaultColor(Transform targetTransform) {
        foreach (Transform t in transform) {
            t.Find("label").GetComponent<TextMeshProUGUI>().color = Color.white;
            t.GetComponent<Image>().color = panelOff;
        }
        targetTransform.Find("label").GetComponent<TextMeshProUGUI>().color = Color.black;
        targetTransform.GetComponent<Image>().color = panelOn;
    }

    private void GetNext() {
        // maybe add a recursion callcount limit here in case all selectors are ever disabled
        currentSelection = (currentSelection + 1) % order.Count;
        if (!IsActive()) {
            GetNext();
        }
    }

    private void GetPrevious() {
        // maybe add a recursion callcount limit here in case all selectors are ever disabled
        currentSelection = currentSelection - 1 < 0 ? order.Count - 1 : currentSelection - 1;
        if (!IsActive()) {
            GetPrevious();
        }
    }

    private bool IsActive() {
        return transform.Find(order[currentSelection]).gameObject.active;
    }

}
