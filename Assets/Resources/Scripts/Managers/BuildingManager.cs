using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingManager : MonoBehaviour {

    public static BuildingManager manager;

    public Dictionary<string, Dictionary<string, int>> productionCost;
    public Dictionary<string, Dictionary<string, int>> buildCost;
    private bool instantiating;
    private Vector3 v3OrgMouse;
    private Plane plane = new Plane(Vector3.up, Vector3.zero);

    void Awake() {
        // singleton pattern
        if (manager == null) {
            manager = this;
        } else if (manager != this) {
            Destroy(gameObject);
        }
        GenerateBuildings();
    }

    void Start() {
        Invoke("PlaceStarterBuildings", 0.1f);
    }

    private void GenerateBuildings() {
        productionCost = new Dictionary<string, Dictionary<string, int>>();
        productionCost.Add("sawyer", new Dictionary<string, int>() {{"wood", 1}});
        productionCost.Add("storage", new Dictionary<string, int>() {{"wood", 0}});
        buildCost = new Dictionary<string, Dictionary<string, int>>();
        buildCost.Add("sawyer", new Dictionary<string, int>() {{"wood", 1}, {"rock", 1}});
        buildCost.Add("storage", new Dictionary<string, int>() {{"wood", 2}});
    }

    public void PlaceStarterBuildings() {
        instantiating = true;
        PlaceBuilding("storage");
        instantiating = false;
    }

    public void PlaceBuilding(string targetType) {
        Object targetPrefab = BuildingPrefabs.buildings.inProgressSprites[targetType];
        if (targetPrefab == null) {
            print("no prefab found for " + targetType);
        }
        InstantiateBuildingObject(targetType, targetPrefab);
    }

    bool CanConstruct(Dictionary<string, int> materialsNeeded) {
        foreach (KeyValuePair<string, int> entry in materialsNeeded) {
            if (entry.Value > ResourceCounter.counter.counts[entry.Key]) {
                return false;
            }
        }
        return true;
    }

    string GetMaterialsRepr(Dictionary<string, int> materialsNeeded) {
        string repr = "";
        foreach (KeyValuePair<string, int> entry in materialsNeeded) {
            repr += entry.Key + ": " + entry.Value + ", ";
        }
        return repr;
    }

    void InstantiateBuildingObject(string targetType, Object targetPrefab) {
        Vector3 placementLocation = new Vector3(0f, 0.5f, 0f);
        if (Input.GetMouseButtonDown(0)) {
            placementLocation = CursorManager.manager.GetPlacementLocation();
        }
        if (AlreadyPlaced(placementLocation)) {
            return;
        }
        GameObject theObject = Instantiate(targetPrefab, placementLocation, Quaternion.identity) as GameObject;
        theObject.GetComponent<Building>().SetConsumes(buildCost[targetType]);
        theObject.GetComponent<Building>().SetName(targetType);
        TargetBucket.bucket.targets.Add(theObject);
        theObject.transform.SetParent(transform);
        theObject.name = targetType;
        if (instantiating) {
            theObject.GetComponent<Building>().BuildIt();
        }
    }

    bool AlreadyPlaced(Vector3 placementLocation) {
        foreach (Transform child in transform) {
            if (child.position == placementLocation) {
                return true;
            }
        }
        return false;
    }
}
